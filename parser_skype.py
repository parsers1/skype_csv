"""
Из Skype экспортируем csv файл - contacts.csv
Складываем любое кол-во csv файлов в подпапки USERS. Имя подпапки это НАШ SKYPE ID
Скрипт перебирает все файлы из подпапок и создает xlsx файл. На каждом отдельном листе будет содержаться книга контактов
"""
import csv
import os.path
import re
import openpyxl as xlsx
from openpyxl.styles import Font, Color, colors

INPUT_FILE = 'contacts.csv'             #имя csv файла c контактами
EXCEL_FILE = 'contacts_skype.xlsx'      #имя конечного excel файла с контактами
DIR_PREFIX_USERS = 'INPUT/'             #директория с подпапками пользовательских json файлов
DIR_PREFIX_OUTPUTFILE = ''        #директория с конечным excel файлом

# функция удаляет файл по указанному имени в указанной директории
def file_deleter(filename, filepath=''):
    if filepath and (not os.path.isdir(re.sub(r'[/, \, //, \\]', '', filepath))):
        os.mkdir(re.sub(r'[/, \, //, \\]', '', filepath))
    if os.path.isfile(filepath + filename):
        os.remove(filepath + filename)

# функция открывает/создает excel_file возвращает объект openpyxl workbook и добавляет лист с именем sheetname
def excel_opener(excel_file, sheetname='SHEET1', filepath=''):
    if not os.path.isfile(filepath + excel_file):
        excel_file = xlsx.Workbook()
        excel_file.active.title = sheetname
    else:
        excel_file = xlsx.load_workbook(filepath + excel_file)
        excel_file.create_sheet(sheetname)
    return excel_file


if __name__ == '__main__':
    # 1. зачищаем файл экспорта контактов и считываем подпапки в директории USERS, где должны быть csv файлы
    file_deleter(EXCEL_FILE, DIR_PREFIX_OUTPUTFILE)

    # 2. перебираем папки с CSV файлами
    users_dir = os.listdir('./{}'.format(DIR_PREFIX_USERS))
    for users in range(len(users_dir)):
        with open('{}/{}/{}'.format(DIR_PREFIX_USERS, users_dir[users], INPUT_FILE), 'r',  encoding='utf8') as file:
            csv_file = csv.reader(file, delimiter=',')

            # 3. создаем excel файлик и лист с названием пользователя
            sheetname = f'{users_dir[users]}'
            excel_file = excel_opener(EXCEL_FILE, sheetname, DIR_PREFIX_OUTPUTFILE)
            excel_worksheet = excel_file[sheetname]

            # 4. парсим csv и заполняем наш xlsx файл
            counter = 0
            for row in csv_file:
                excel_worksheet[f'B{counter + 3}'] = row[1]
                excel_worksheet[f'A{counter + 3}'] = row[2]
                counter += 1

            # разметка заголовков страницы excel
            excel_worksheet['A1'] = 'НАШ SKYPE ID: ' + users_dir[users]
            excel_worksheet['A2'] = 'ИМЯ КЛИЕНТА'
            excel_worksheet['B2'] = 'SKYPE ID КЛИЕНТА'

            # форматирование ячеек
            excel_worksheet['A1'].font = Font(color=colors.BLUE, bold=True)
            excel_worksheet['A2'].font = Font(bold=True)
            excel_worksheet['B2'].font = Font(bold=True)
            excel_worksheet.column_dimensions['A'].width = 41
            excel_worksheet.column_dimensions['B'].width = 35

            # 5. сохранение
            excel_file.save(DIR_PREFIX_OUTPUTFILE + EXCEL_FILE)